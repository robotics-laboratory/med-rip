extends Spatial

var worker: Worker
var counter: float = 0.0
var thread

onready var robot = get_node("Robot")

func _ready():
	worker = Worker.new()
	thread = Thread.new()
	thread.start(worker, "work")
	print("Spawned!")

func _process(delta):
	robot.generalized = worker.axis
