extends Object
class_name Worker

var stopper: Mutex
var shall_stop: bool = false
var axis: Array = [
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
]

func _init():
	stopper = Mutex.new()

func work(_data) -> void:
	var shall_continue = true
	
	var tcp_stream = StreamPeerTCP.new()
	tcp_stream.connect_to_host("127.0.0.1", 31337)
	
	var buffer = ""
	
	while shall_continue:
		stopper.lock()
		shall_continue = !shall_stop
		stopper.unlock()
		
		if tcp_stream.get_available_bytes() > 0:
			var byte = char(tcp_stream.get_u8())
			if byte == '\n':
				parse_buffer(buffer)
				buffer = ""
			else:
				buffer += byte

func parse_buffer(buffer):
	var data = JSON.parse(buffer)
	axis = data.result.axis

func stop() -> void:
	stopper.lock()
	shall_stop = true
	stopper.unlock()
