extends Spatial

export var generalized: Array = [
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
]

export var bones: Array = []
var _bones: Array = [
	null,
	null,
	null,
	null,
	null,
	null,
	null,
]

func _ready():
	for i in range(1, len(bones)):
		_bones[i-1] = get_node(bones[i])


func _process(delta):
	_bones[0].rotation = Vector3(0, 0, generalized[0])
	_bones[1].rotation = Vector3(0, generalized[1], 0)
	_bones[2].rotation = Vector3(0, 0, generalized[2])
	_bones[3].rotation = Vector3(0, -generalized[3], 0)
	_bones[4].rotation = Vector3(0, 0, generalized[4])
	_bones[5].rotation = Vector3(0, generalized[5], 0)
	_bones[6].rotation = Vector3(0, 0, generalized[6])
